﻿using EssentialTools2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;

namespace EssentialTools2.Controllers
{
    public class HomeController : Controller
    {
        IValueCalculator calc;
        List<Product> products = new List<Product> {        new Product {Name = "Kayak",        Category = "Watersports",   Price = 275M },
                                                            new Product {Name = "Lifejacket",   Category = "Watersports",   Price = 48.95M},
                                                            new Product {Name = "Soccer ball",  Category = "Soccer",        Price = 19.50M},
                                                            new Product {Name = "Corner flag",  Category = "Soccer",        Price = 34.95M}
                                                        };

        public HomeController(IValueCalculator calc)
        {
            this.calc = calc;
        }        
        // GET: Home
        public ActionResult Index()
        {
                            //var kernel = new StandardKernel();
                            //kernel.Bind<IValueCalculator>().To<LinqValueCalculator>();

                            //IValueCalculator calc = kernel.Get<IValueCalculator>();
            var cart = new ShoppingCart(calc) { Products = products };
            decimal totalValue = cart.CalculateProductTotal();
            return View(totalValue);

        }
    }
}